package cz.glubo

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldNotBe
import io.micronaut.test.annotation.TransactionMode
import io.micronaut.test.extensions.kotest5.annotation.MicronautTest
import kotlinx.coroutines.flow.toCollection

@MicronautTest(transactionMode = TransactionMode.SEPARATE_TRANSACTIONS)
class BooksTest(
    private val bookRepository: BookRepository,
) : StringSpec({
        "trala" {
            bookRepository.findAll().toCollection(mutableListOf()) shouldNotBe null
        }
    })
