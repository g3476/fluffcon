package cz.glubo

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.micronaut.runtime.EmbeddedApplication
import io.micronaut.test.extensions.kotest5.annotation.MicronautTest

@MicronautTest(transactional = false)
class FluffconTest(private val application: EmbeddedApplication<*>) : StringSpec({

    "test the server is running" {
        application.isRunning shouldBe true
    }
})
