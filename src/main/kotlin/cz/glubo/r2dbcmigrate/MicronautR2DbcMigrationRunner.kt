package cz.glubo.r2dbcmigrate

import io.github.oshai.kotlinlogging.KotlinLogging
import io.micronaut.context.event.BeanCreatedEvent
import io.micronaut.context.event.BeanCreatedEventListener
import io.micronaut.core.io.ResourceLoader
import io.r2dbc.spi.ConnectionFactory
import jakarta.inject.Singleton
import name.nkonev.r2dbc.migrate.core.R2dbcMigrate
import name.nkonev.r2dbc.migrate.core.R2dbcMigrateProperties
import name.nkonev.r2dbc.migrate.reader.MigrateResource
import name.nkonev.r2dbc.migrate.reader.MigrateResourceReader
import java.io.File
import java.io.InputStream
import kotlin.streams.asSequence

@Singleton
class MicronautR2DbcMigrationRunner(
    private val resourceLoader: ResourceLoader,
) : BeanCreatedEventListener<ConnectionFactory> {
    private val logger = KotlinLogging.logger {}

    override fun onCreated(event: BeanCreatedEvent<ConnectionFactory>): ConnectionFactory {
        logger.warn { "created $event" }
        R2dbcMigrate.migrate(
            event.bean,
            R2dbcMigrateProperties().apply {
                resourcesPaths = listOf("db/migration")
                connectionMaxRetries = 5
            },
            MicronautResourceReader(resourceLoader),
            null,
            null,
        ).block()

        return event.bean
    }

    class MicronautResourceReader(
        private val resourceLoader: ResourceLoader,
    ) : MigrateResourceReader {
        override fun getResources(resourcesPath: String): List<MigrateResource> {
            return resourceLoader.getResources(resourcesPath)
                .asSequence()
                .flatMap {
                    val directory = File(it.toURI())
                    val a =
                        directory.listFiles()?.map { file ->
                            MicronautMigrateResource(file)
                        } ?: emptyList()
                    a
                }.toList()
        }

        class MicronautMigrateResource(private val file: File) : MigrateResource {
            override fun isReadable() = file.canRead()

            override fun getInputStream(): InputStream = file.inputStream()

            override fun getFilename() = file.name
        }
    }
}
