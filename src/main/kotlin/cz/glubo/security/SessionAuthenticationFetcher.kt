package cz.glubo.security

import io.micronaut.context.annotation.Infrastructure
import io.micronaut.http.HttpRequest
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.authentication.ClientAuthentication
import io.micronaut.security.filters.AuthenticationFetcher
import org.reactivestreams.Publisher
import reactor.core.publisher.Mono
import kotlin.jvm.optionals.getOrNull

@Infrastructure
class SessionAuthenticationFetcher() : AuthenticationFetcher<HttpRequest<Any>> {
    private val admins = listOf("GluboTheMad")

    override fun fetchAuthentication(request: HttpRequest<Any>): Publisher<Authentication> {
        return Mono.create { emitter ->
            val username =
                request.headers.getFirst("X-Forwarded-Preferred-Username")
                    .getOrNull()
            val userId =
                request.headers.getFirst("X-Forwarded-User")
                    .getOrNull()
            emitter.success(
                username?.let {
                    ClientAuthentication(
                        it,
                        mapOf(
                            "roles" to if (username in admins) listOf("ROLE_ADMIN") else listOf(),
                            "userId" to userId,
                        ),
                    )
                },
            )
        }
    }
}
