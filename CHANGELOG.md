# Changelog


## 0.11.1 (2023-11-20)

### fix

* [47782] fix: disable security for /health (Petr Sykora)

