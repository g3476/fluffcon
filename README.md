# Fluffcon

## Requirements

- MariaDb, configurable by env vars with following defaults
    - MARIADB_HOST: 127.0.0.1
    - MARIADB_PORT: 13306
    - MARIADB_DATABASE: fluffcon
    - MARIADB_USERNAME: fluffcon
    - MARIADB_PASSWORD: velmitajneheslo
- for tests `fluffcon-test` database is used by default
- Edge proxy dealing with security, filling headers
    - `X-Forwarded-Preferred-Username` -- display name of authenticated user
    - `X-Forwarded-User` -- unique id of the user (can be a string up to 128 characters long)

## Development Auth Proxy

I personally use mitmproxy inside podman to fake the headers (one user per instance, different ports)

```shell
podman run --rm \
 --name mitma \
 -p 1234:8080 \
 -p 2234:8081 \
 mitmproxy/mitmproxy \
 -- \
 mitmweb \
 --web-host 0.0.0.0 \
 --mode reverse:http://host.containers.internal:8080 \
 --modify-headers /X-Forwarded-Preferred-Username/Borg \
 --modify-headers /X-Forwarded-User/321
```

With this example, you can access the proxied application at http://127.0.0.1:1234/ and see intercepted request/responses at mitm web ui
at http://127.0.0.1:2234/ .

You can probably replace `podman` with `docker` and `host.containers.internal` with `host.docker.internal` to use 1docker instead of podman.

## Production 