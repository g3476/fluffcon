import gg.jte.gradle.GenerateJteTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.8.22"
    id("org.jetbrains.kotlin.plugin.allopen") version "1.8.22"
    id("com.google.devtools.ksp") version "1.8.22-1.0.11"
    id("com.github.johnrengelman.shadow") version "8.1.1"
    id("io.micronaut.application") version "4.0.4"
    id("io.micronaut.aot") version "4.0.4"
    id("org.jlleitschuh.gradle.ktlint") version "11.6.1"
    id("com.google.cloud.tools.jib") version "3.3.2"
    id("gg.jte.gradle") version "2.3.2"
}

group = "cz.glubo"

val kotlinVersion = project.properties.get("kotlinVersion")
repositories {
    mavenCentral()
}

dependencies {
    ksp("io.micronaut.data:micronaut-data-processor")
    ksp("io.micronaut:micronaut-http-validation")
    ksp("io.micronaut.serde:micronaut-serde-processor")
    implementation("io.micronaut:micronaut-management")
    implementation("io.micronaut.data:micronaut-data-r2dbc")
    implementation("io.micronaut.kotlin:micronaut-kotlin-runtime")
    implementation("io.micronaut.serde:micronaut-serde-jackson")
    implementation("io.micronaut.views:micronaut-views-jte")
    implementation("gg.jte:jte-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.3")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactive:1.7.3")
    implementation("name.nkonev.r2dbc-migrate:r2dbc-migrate-core:2.10.3")
    implementation("io.github.oshai:kotlin-logging-jvm:5.1.0")
    compileOnly("io.micronaut:micronaut-http-client")
    runtimeOnly("ch.qos.logback:logback-classic")
    runtimeOnly("com.fasterxml.jackson.module:jackson-module-kotlin")
    runtimeOnly("org.mariadb:r2dbc-mariadb")
    runtimeOnly("io.r2dbc:r2dbc-pool")
    runtimeOnly("org.yaml:snakeyaml")
    testImplementation("io.micronaut:micronaut-http-client")
    aotPlugins("io.micronaut.security:micronaut-security-aot:3.9.0")
    ksp("io.micronaut.security:micronaut-security-annotations")
    implementation("io.micronaut.security:micronaut-security")
    implementation("io.github.serpro69:kotlin-faker:1.15.0")
}

application {
    mainClass.set("cz.glubo.ApplicationKt")
}
java {
    sourceCompatibility = JavaVersion.toVersion("17")
}

tasks {
    compileKotlin {
        compilerOptions {
            jvmTarget.set(org.jetbrains.kotlin.gradle.dsl.JvmTarget.JVM_17)
        }
    }
    compileTestKotlin {
        compilerOptions {
            jvmTarget.set(org.jetbrains.kotlin.gradle.dsl.JvmTarget.JVM_17)
        }
    }
}
graalvmNative.toolchainDetection.set(false)
micronaut {
    runtime("netty")
    testRuntime("kotest5")
    processing {
        incremental(true)
        annotations("cz.glubo.*")
    }
    aot {
        // Please review carefully the optimizations enabled below
        // Check https://micronaut-projects.github.io/micronaut-aot/latest/guide/ for more details
        optimizeServiceLoading.set(false)
        convertYamlToJava.set(false)
        precomputeOperations.set(true)
        cacheEnvironment.set(true)
        optimizeClassLoading.set(true)
        deduceEnvironment.set(false)
        optimizeNetty.set(true)
    }
}

jib {
    container {
        user = "1002"
    }
}

jte {
    generate()
}

tasks.withType<KotlinCompile>().configureEach {
    dependsOn(tasks.withType<GenerateJteTask>())
}
tasks.withType<Jar>().configureEach {
    dependsOn(tasks.withType<GenerateJteTask>())
}
tasks.named("inspectRuntimeClasspath") {
    dependsOn(tasks.withType<GenerateJteTask>())
}
tasks.named("runKtlintFormatOverMainSourceSet") {
    dependsOn(tasks.withType<GenerateJteTask>())
}
tasks.named("runKtlintCheckOverMainSourceSet") {
    dependsOn(tasks.withType<GenerateJteTask>())
}

ktlint {
    version = "1.0.0"
}
